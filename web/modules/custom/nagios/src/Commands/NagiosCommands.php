<?php

namespace Drupal\nagios\Commands;

use Drush\Commands\DrushCommands;

class NagiosCommands extends DrushCommands {

  /**
   * @command nagios
   */
  public function nagios() {
    update_cron();
    include_once DRUPAL_ROOT . '/core/includes/install.inc';
    drupal_load_updates();

    $descriptions = array();
    $requirements = \Drupal::moduleHandler()->invokeAll('requirements', array('runtime'));
    $ignore = [
      'Drupal core security coverage',
      'Deprecated modules enabled',
    ];

    foreach ($requirements as $key => $requirement) {
      if (in_array($requirement['title'], $ignore) && $requirement['severity'] == REQUIREMENT_WARNING) {
        continue;
      }

      if (isset($requirement['severity']) && $requirement['severity'] > REQUIREMENT_OK) {
        $descriptions[] = $requirement['title'];
      }
    }

    if (!empty($descriptions)) {
      $this->output()->writeln('DRUPAL WARNING - ' . implode(', ', $descriptions));
      return 1;
    }
    else {
      $this->output()->writeln('DRUPAL OK');
      return 0;
    }
  }

}
