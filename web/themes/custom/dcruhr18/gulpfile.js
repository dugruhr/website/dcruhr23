var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var newer = require('gulp-newer');
var runsequence = require('run-sequence');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var argv = require('yargs').argv;
var gulpif = require('gulp-if');
var production = true;
var assets_src = '../../../../vendor/twbs/bootstrap-sass/assets/**/*';
var assets_dst = './bootstrap/assets';
var styles_src = './scss/**/*.scss';
var styles_dst = './css';

gulp.task('sync', function() {
  return gulp.src(assets_src)
    .pipe(newer(assets_dst))
    .pipe(gulp.dest(assets_dst));
});

gulp.task('sass', function () {
  return gulp.src(styles_src)
    .pipe(gulpif(!production, sourcemaps.init()))
      .pipe(sass({outputStyle: production ? 'compressed' : 'expanded'}))
      .pipe(autoprefixer())
    .pipe(gulpif(!production, sourcemaps.write()))
    .pipe(gulp.dest(styles_dst));
});

gulp.task('build', function(cb) {
  runsequence('sync', 'sass', cb);
});

gulp.task('watch', ['build'], function () {
  gulp.watch(styles_src, ['sass']);
});

gulp.task('default', ['build']);
