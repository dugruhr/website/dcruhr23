(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.dcruhr18 = {
    attach: function (context, settings) {

      $('.navbar ul.navbar-nav > li > a').on('touchstart', function (e) {
        'use strict'; //satisfy code inspectors
        var $linkWrapper = $(this).parent(); //preselect the link
        if ($linkWrapper.hasClass('hover')) {
          return true;
        }
        else {
          $linkWrapper.addClass('hover');
          $('.navbar ul.navbar-nav > li').not($linkWrapper).removeClass('hover');
          e.preventDefault();
          return false; //extra, and to make sure the function has consistent
                        // return points
        }
      });

      $('main').on('touchstart', function (e) {
        $('.navbar ul.navbar-nav > li').removeClass('hover');
      });

      if ($('.mobile-menu').length === 0) {
        this.initMobileMenu(context);
        console.log('initMobileMenu')
      } else {
        if ($('.mobile-menu .language-switcher-language-url').length === 0) {
          if ($(context).find('.language-switcher-language-url').length > 0) {
            $('.mobile-menu').append($(context).find('.language-switcher-language-url')[0].outerHTML);
            console.log('languageswitcherFromContext')
          }
          if ($('.overlay-right-wrapper .language-switcher-language-url').length > 0) {
            $('.mobile-menu').append($('.overlay-right-wrapper .language-switcher-language-url')[0].outerHTML);
            console.log('languageswitcherFromOverlay')
          }
        }
      }

    },

    initMobileMenu: function (context) {

      $('.navbar').before('<a href="#" class="menutoggle"><div class="toggle"><span></span><span></span><span></span><span></span><span class="visually-hidden">Menü</span></div></a><div class="mobile-menu"><h3><a href="/" title="Home">#DCRUHR23</a></h3></div>');
      //$('.mobile-menu').append($('#block-suchformular')[0].outerHTML);
      $('.mobile-menu').append($('#block-hauptnavigation')[0].outerHTML);
      if ($('.overlay-right-wrapper .language-switcher-language-url').length > 0) {
        $('.mobile-menu').append($('.overlay-right-wrapper .language-switcher-language-url')[0].outerHTML);
      }

      //$('.mobile-menu').append($('.partners')[0].outerHTML);

      $('.mobile-menu ul.menu li.menu-item--expanded').each(function () {
        $(this).prepend('<span class="opentoggle"></span>');
        $(this).children('.opentoggle').unbind('click').click(function () {
          $(this).parent().toggleClass('menu-item--active-trail');
        });
      });

      $('.menutoggle').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('open').find('.toggle').toggleClass('open');
        $('.mobile-menu').toggleClass('open');
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
